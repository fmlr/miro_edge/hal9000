![HAL 9000](HAL9000_Case.svg)

# Integration Guide for miro EdgeCard v0.1.0
## Overview

![Block](block.png)

## Theory of operation

The miro EdgeCard implements a standard ACM device (virtual serial character device) for the communcation with the host. The Linux driver needed is _cdc_acm_.
The software stack on the host side, as provided by Semtech and modified by Miromico, consists of two parts:
- the HAL library to provide a generic way to access the hardware
- the packet forwarder using the HAL, which implements a simple protocol to forward radio packets to a server (and vice versa).
Some LoRaWAN network stacks implement their own custom packet forwarder or have a local daemon running to which the packet forwarder connects to. The intention is to mitigate the shortcomings of the Semtech packet forwarder (such as problems with NAT traversal). Hence it is often referred to as "_legacy_ packet forwarder".   

## Initial operation

A first system check should be done with _lsusb_ or _/sys/kernel/debug/usb/devices_ etc. to check for a matching device on the USB bus:
```
P:  Vendor=0483 ProdID=5740 Rev= 2.00
S:  Manufacturer=STMicroelectronics
S:  Product=SEMTECH PicoGW Virtual ComPort
```
The Linux driver should have created a /dev/ttyACMx device.

## Source code
### Notes

Make sure to use the "integration" branch:

```
git checkout integration
```

If you build against libmusl, use the "integration-musl" branch.

### Build

Both components (HAL and packet forwarder) have their own Makefile where $(CROSS_COMPILE) etc. can be set for standalone builds.
The HAL layer is built as static library and then static linked in the packet forwarder.
As an example for integration in a custom build system, the openwrt directory in the sources holds a Makefile for the OpenWRT distribution.

### Running the packet forwarder

The forwarder has a single configuration file, _global_conf.json_, which specifies, among others,
- the center frequencies of the two radio frontends
- the frequency difference to the actual receive frequency for each receive channel
- the server host name and port
- the _gateway id_, a "unique" 8 Byte number to identifiy the gateway. This is usually derived with a simple script from the MAC address of the network interface of the gateway.
The picoGW_packet_forwarder/lora_pkt_fwd directory holds a few examples of these config files.
Example call for global_conf.json in /opt and the gateway card on /dev/ttyACM1:
```
lora_pkt_fwd -d /dev/ttyACM1 -c /opt
```
(defaults are /dev/ttyACM0 and the current path for the config file)

### Miromico additions to the Semtech provided sources
- SLIP encoding of the characters over the (virtual) serial line to prevent synchronization loss 
- additional commands to configure the RF frontend and switch the antenna path (internal/external antenna), depending on the specific hardware version. In order to change the config to external antenna, use the following command: `/opt/util_settings -w 1,1,1 -d <path/to/tty>` and reset the gateway card.
