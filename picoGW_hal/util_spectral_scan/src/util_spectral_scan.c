/*
  ______                              _
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \
 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
  (C)2014 Semtech-Cycleo

Description:
    SX1301 spectral scan

*/


/* -------------------------------------------------------------------------- */
/* --- DEPENDENCIES --------------------------------------------------------- */

/* Fix an issue between POSIX and C99 */
#if __STDC_VERSION__ >= 199901L
    #define _XOPEN_SOURCE 600
#else
    #define _XOPEN_SOURCE 500
#endif

#include <stdint.h>     /* C99 types */
#include <stdio.h>      /* NULL printf */
#include <stdlib.h>     /* EXIT atoi */
#include <unistd.h>     /* getopt */
#include <string.h>
#include "loragw_com.h"
#include "loragw_hal.h"
#include "loragw_reg.h"
#include "loragw_aux.h"
#include "loragw_mcu.h"
/* -------------------------------------------------------------------------- */
/* --- MACROS & CONSTANTS --------------------------------------------------- */

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))
#define DEFAULT_START_FREQ          864000000   /* start frequency, Hz */
#define DEFAULT_STOP_FREQ           870000000   /* stop frequency, Hz */
#define DEFAULT_STEP_FREQ           100000      /* frequency step, Hz */
#define DEFAULT_RSSI_PTS            1000       /* number of RSSI reads */
#define DEFAULT_LOG_NAME            "rssi_histogram"

#define RSSI_RANGE                  256

#define MAX_FREQ                    1000000000
#define MIN_FREQ                    800000000
#define MIN_STEP_FREQ               5000

#define COM_PATH_DEFAULT        "/dev/ttyACM0"
/* -------------------------------------------------------------------------- */
/* --- GLOBAL VARIABLES ----------------------------------------------------- */

/* -------------------------------------------------------------------------- */
/* --- MAIN FUNCTION -------------------------------------------------------- */

int main( int argc, char ** argv )
{
    int i, j; /* loop and temporary variables */
    int x = 0; /* return code for functions */


    /* Parameter parsing */
    float arg_lf[3] = {0,0,0};
    unsigned arg_u = 0;
    char arg_s[64];

    /* Application parameters */
    uint32_t start_freq = DEFAULT_START_FREQ;
    uint32_t stop_freq = DEFAULT_STOP_FREQ;
    uint32_t step_freq = DEFAULT_STEP_FREQ;
    uint16_t rssi_pts = DEFAULT_RSSI_PTS;
    char log_file_name[64] = DEFAULT_LOG_NAME;
    FILE * log_file = NULL;

    /* Local var */

    int freq_nb = 32;

    uint32_t freq;
    uint8_t rssi_value [32] ;
    /* COM interfaces */
    const char com_path_default[] = COM_PATH_DEFAULT;
    const char *com_path = com_path_default;
    /* Parse command line options */
    while((i = getopt(argc, argv, "hf:n:l:d:")) != -1) {
        switch (i) {
        case 'h':
            printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
            printf(" -f <float>:<float>:<float>  Frequency vector to scan in MHz (start:step:stop)\n");
            printf("                               start>%3.3f step>%1.3f stop>%1.3f\n", MIN_FREQ/1e6, MIN_STEP_FREQ/1e6,DEFAULT_STOP_FREQ/1e6);
            printf(" -d <path> COM device to be used to access the concentrator board\n");
            printf("            => default path: " COM_PATH_DEFAULT "\n");
            printf(" -n <uint>  Total number of RSSI points [1..65535]\n");
            printf(" -l <char>  Log file name\n");
            printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
            return EXIT_SUCCESS;

        case 'f': /* -f <float>:<float>:<float>  Frequency vector to scan in MHz, start:step:stop */
            j = sscanf(optarg, "%f:%f:%f", &arg_lf[0], &arg_lf[1], &arg_lf[2]);
            if ((j!=3) || (arg_lf[0] < MIN_FREQ/1e6) || (arg_lf[0] > MAX_FREQ/1e6) || (arg_lf[1] < MIN_STEP_FREQ/1e6) || (arg_lf[2] < MIN_FREQ/1e6) || (arg_lf[2] > MAX_FREQ/1e6) || (arg_lf[2]<(arg_lf[0])))  {
                printf("ERROR: argument parsing of -f argument. -h for help.\n");
                return EXIT_FAILURE;
            } else {
                start_freq = (uint32_t)((arg_lf[0] * 1e6) + 0.5); /* .5 Hz offset to get rounding instead of truncating */
                step_freq  = (uint32_t)((arg_lf[1] * 1e6) + 0.5); /* .5 Hz offset to get rounding instead of truncating */
                stop_freq  = (uint32_t)((arg_lf[2] * 1e6) + 0.5); /* .5 Hz offset to get rounding instead of truncating */
            }
            break;

        case 'n': /* -n <uint>  Total number of RSSI points [1..65535] */
            j = sscanf(optarg, "%u", &arg_u);
            if ((j != 1) || (arg_u < 1) || (arg_u > 65535)) {
                printf("ERROR: argument parsing of -n argument. -h for help.\n");
                return EXIT_FAILURE;
            } else {
                rssi_pts = (uint16_t)arg_u;
            }
            break;

        case 'l': /* -l <char>  Log file name */
            j = sscanf(optarg, "%s", arg_s);
            if (j != 1) {
                printf("ERROR: argument parsing of -l argument. -h for help.\n");
                return EXIT_FAILURE;
            } else {
                sprintf(log_file_name, "%s", arg_s);
            }
            break;
        case 'd':
            if (optarg != NULL) {
                com_path = optarg;
            }
            break;

        default:
            printf("ERROR: argument parsing options. -h for help.\n");
            return EXIT_FAILURE;
        }
    }

    /* Start message */
    printf("+++ Start spectral scan of LoRa gateway channels +++\n");

    i = lgw_connect(com_path);
    if(i != 0) {
        printf("ERROR: Failed to connect \n");
        return EXIT_FAILURE;
    }
    printf("start spectral scan config \n");
    i = lgw_mcu_config_spectral_scan( start_freq, step_freq, rssi_pts); 
    //i=0;
    if(i != 0) {
        printf("ERROR: Failed to config spectral scan \n");
        return EXIT_FAILURE;
    }
    sleep(3);
    printf("end spectral scan config \n");
    /* create log file */
    strcat(log_file_name,".csv");
    log_file = fopen(log_file_name, "w");
    if (log_file == NULL) {
        printf("ERROR: impossible to create log file %s\n", log_file_name);
        return EXIT_FAILURE;
    }
    printf("Writing to file: %s\n", log_file_name);

    /* Number of frequency steps */
    freq_nb = (stop_freq - start_freq) / step_freq ;
    printf("Scanning frequencies:\nstart: %d Hz\nstop : %d Hz\nstep : %d Hz\nnb   : %d\n", start_freq, stop_freq, step_freq, freq_nb);

    /* Main loop */
    for(j = 0; j < freq_nb; j++) {
        /* Current frequency */
        freq = start_freq + j * step_freq;
        printf("test freq %d\n", freq);
        /* Write data to CSV */
        fprintf(log_file, "%d", freq);
        while(lgw_mcu_get_spectral_scan(j , &rssi_value[0])){
            printf("Warning : spectral scan not yet ready \n");
            sleep(3);
        }
        //lgw_mcu_get_spectral_scan(j , &rssi_value[0]);

        for (i = 0; i < 32; i++) {
            fprintf(log_file, ",%d,%d", -27 -(i*3), rssi_value[i]);
            printf(",%d,%d", -27 -(i*3), rssi_value[i]);
        }
        fprintf(log_file, "\n");
        printf("\n");
    }
    fclose(log_file);

    /* Close SPI */
    x = lgw_disconnect();
    if(x != 0) {
        printf("ERROR: Failed to disconnect FPGA\n");
        return EXIT_FAILURE;
    }

    printf("+++  Exiting Spectral scan program +++\n");

    return EXIT_SUCCESS;
}

/* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ */

/* --- EOF ------------------------------------------------------------------ */

