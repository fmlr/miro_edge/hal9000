/*
 / _____)             _              | |
( (____  _____ ____ _| |_ _____  ____| |__
 \____ \| ___ |    (_   _) ___ |/ ___)  _ \

 _____) ) ____| | | || |_| ____( (___| | | |
(______/|_____)_|_|_| \__)_____)\____)_| |_|
  (C)2017 Semtech-Cycleo

Description:
 Utility to set antenna switch etc 

License: Revised BSD License, see LICENSE.TXT file include in the project
*/


/* -------------------------------------------------------------------------- */
/* --- DEPENDANCIES --------------------------------------------------------- */

/* fix an issue between POSIX and C99 */
#if __STDC_VERSION__ >= 199901L
#define _XOPEN_SOURCE 600
#else
#define _XOPEN_SOURCE 500
#endif

#include <stdint.h>     /* C99 types */
#include <stdio.h>      /* printf fprintf sprintf fopen fputs */
#include <unistd.h>     /* getopt access */
#include <stdlib.h>     /* EXIT_FAILURE */

#include "loragw_com.h"

/* -------------------------------------------------------------------------- */
/* --- PRIVATE MACROS ------------------------------------------------------- */

#define ARRAY_SIZE(a)    (sizeof(a) / sizeof((a)[0]))
#define MSG(args...)    fprintf(stderr, args) /* message that is destined to the user */

/* -------------------------------------------------------------------------- */
/* --- PRIVATE CONSTANTS ---------------------------------------------------- */

#define COM_PATH_DEFAULT "/dev/ttyACM0"

/* -------------------------------------------------------------------------- */
/* --- PRIVATE VARIABLES (GLOBAL) ------------------------------------------- */

void *lgw_com_target = NULL; /*! generic pointer to the COM device */

/* -------------------------------------------------------------------------- */
/* --- PRIVATE FUNCTIONS DECLARATION ---------------------------------------- */

void usage (void);

/* -------------------------------------------------------------------------- */
/* --- PRIVATE FUNCTIONS DEFINITION ----------------------------------------- */

/* describe command line options */
void usage(void) {
    MSG("Available options:\n");
    MSG(" -h print this help\n");
    MSG(" -d <path> COM device to be used to access the concentrator board\n");
    MSG("            => default path: " COM_PATH_DEFAULT "\n");
    MSG(" -w <ant>,<led_select>,<led_tx>\n");
    MSG("  where <ant>        [0|1] internal(0) or external (1) antenna\n");
    MSG("        <led_select> [0|1] use LED_WWAN (0) or LED_WPAN (1)\n");
    MSG("        <led_tx>     [0|1] LED on TX (0) or via GPIO (1)\n");
    MSG(" without -w parameter: Just print out the current values.\n");
}

/* -------------------------------------------------------------------------- */
/* --- MAIN FUNCTION -------------------------------------------------------- */

int i, x;
unsigned int ant, led_sel, led_gpio, do_write;;
lgw_com_cmd_t cmd;
lgw_com_ans_t ans;
/* COM interfaces */
const char com_path_default[] = COM_PATH_DEFAULT;
const char *com_path = com_path_default;

int getparams(){
    cmd.id = '1';
	cmd.len_msb = 0;
	cmd.len_lsb = 0;
	cmd.address = 0;
	/* send command to MCU */
	x = lgw_com_send_command(lgw_com_target, cmd, &ans);
	if (x == LGW_COM_ERROR || ans.status != ACK_OK) {
		printf("ERROR: FAIL TO SEND COMMAND\n");
		return -1;
	} return 0;
}

int setparams(){
	cmd.cmd_data[0] = ant;
	cmd.cmd_data[1] = led_sel;
	cmd.cmd_data[2] = led_gpio;
	cmd.id = '0';
	cmd.len_msb = 0;
	cmd.len_lsb = 3;
	cmd.address = 0;
	/* send command to MCU */
	x = lgw_com_send_command(lgw_com_target, cmd, &ans);
	if (x == LGW_COM_ERROR) {
		printf("ERROR: FAIL TO SEND COMMAND\n");
		return -1;
	} return 0;
}

int main(int argc, char **argv) {

	ant = 0;
	led_sel = 0;
	led_gpio = 0;
	do_write = 0;
	cmd.cmd_data[0] = 0xA0; // magic

    while ((i = getopt (argc, argv, "hd:w:")) != -1) {
        switch (i) {
            case 'h':
                usage();
                return EXIT_FAILURE;
                break;

            case 'w':
            	do_write = 1;
            	sscanf( optarg, "%u,%u,%u", &ant, &led_sel, &led_gpio );
            	//printf("chosen: %s %u %u %u\n", optarg, ant, led_sel, led_gpio);
                break;

            case 'd':
                if (optarg != NULL) {
                    com_path = optarg;
                }
                break;

            default:
                MSG("ERROR: argument parsing use -h option for help\n");
                usage();
                return EXIT_FAILURE;
        }
    }



    /* Open communication bridge */
    x = lgw_com_open(&lgw_com_target, com_path);
    if (x == LGW_COM_ERROR) {
        printf("ERROR: FAIL TO CONNECT BOARD ON %s\n", com_path);
        return -1;
    }

    if(getparams()){
    	printf("ERROR: FAIL TO READ OUT BOARD ON %s\n", com_path);
    	return -1;
    }

	if (do_write){
		if (ans.ans_data[0] && ans.ans_data[1] == ant && ans.ans_data[2] == led_sel && ans.ans_data[3] == led_gpio){
			printf("same data as in flash -> do nothing\n");
		} else {
			setparams();
		}
	} else {
		printf("datavalid: %u\n", ans.ans_data[0]);
		printf("ant: %u\n", ans.ans_data[1]);
		printf("led select: %u\n", ans.ans_data[2]);
		printf("led function: %u\n", ans.ans_data[3]);
	}
    /* Close communication bridge */
    x = lgw_com_close(lgw_com_target);
    if (x == LGW_COM_ERROR) {
        printf("ERROR: FAIL TO DISCONNECT BOARD\n");
        return -1;
    }

    return EXIT_SUCCESS;
}

/* --- EOF ------------------------------------------------------------------ */


