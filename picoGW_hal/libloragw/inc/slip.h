/*
 * slip.h
 *
 *  Created on: Jan 7, 2019
 *      Author: rehm
 */

#ifndef PICOGW_HAL_LIBLORAGW_INC_SLIP_H_
#define PICOGW_HAL_LIBLORAGW_INC_SLIP_H_

#include <stdint.h>     /* C99 types */
#include <stdbool.h>    /* bool type */

#define SLIP_TX_BUFFER_SIZE	4096
#define SLIP_RX_BUFFER_SIZE	4096

extern uint8_t slip_tx_buffer[SLIP_TX_BUFFER_SIZE];
extern uint8_t slip_rx_buffer[SLIP_RX_BUFFER_SIZE];

uint32_t slip_encode(uint8_t* src, uint32_t len);
void slip_feed_rx(uint8_t input);
bool slip_rx_is_complete();
void slip_init();

#endif /* PICOGW_HAL_LIBLORAGW_INC_SLIP_H_ */
