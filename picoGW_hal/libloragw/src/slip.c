/*
 * slip.c
 *
 *  Created on: Jan 7, 2019
 *      Author: rehm
 */

#include "slip.h"
#include <stdio.h>      /* printf fprintf */

static uint32_t num_chars;
static bool escaping;
static bool complete;

uint8_t slip_tx_buffer[SLIP_TX_BUFFER_SIZE];
uint8_t slip_rx_buffer[SLIP_RX_BUFFER_SIZE];

/* SLIP special character codes
 */
#define END             0300    /* indicates end of packet */
#define ESC             0333    /* indicates byte stuffing */
#define ESC_END         0334    /* ESC ESC_END means END data byte */
#define ESC_ESC         0335    /* ESC ESC_ESC means ESC data byte */

//#define DBGEN

#ifdef DBGEN
static void prettyPrint(uint8_t c){
	fprintf(stderr, "0x%02x ", c);
}

static void prettyPrintSlip(uint8_t c){
	switch (c){
		case END:
			fprintf(stderr, "END ");
			break;
		case ESC:
			fprintf(stderr, "ESC ");
			break;
		case ESC_END:
			fprintf(stderr, "ESC-END ");
			break;
		case ESC_ESC:
			fprintf(stderr, "ESC-ESC ");
			break;
		default:
			prettyPrint(c);
	}
}
#endif

uint32_t slip_encode(uint8_t* src, uint32_t len){
	uint32_t dstindex = 0;
	// send initial END to flush
	slip_tx_buffer[dstindex++] = END;
	for(uint32_t i = 0; i<len; i++){
		switch (src[i]){
			case END:
				slip_tx_buffer[dstindex++] = ESC;
				slip_tx_buffer[dstindex++] = ESC_END;
				break;
			case ESC:
				slip_tx_buffer[dstindex++] = ESC;
				slip_tx_buffer[dstindex++] = ESC_ESC;
				break;
			default:
				slip_tx_buffer[dstindex++] = src[i];
		}
	}
	slip_tx_buffer[dstindex++] = END;
#ifdef DBGEN
	fprintf(stderr, "\nslip encode: input\n");
	for(uint32_t i = 0; i<len; i++){
		prettyPrint(src[i]);
	}
	fprintf(stderr, "\nslip encode: output\n");
	for(uint32_t i = 0; i<dstindex; i++){
		prettyPrintSlip(slip_tx_buffer[i]);
	}
#endif

	return dstindex;
}

void slip_feed_rx(uint8_t input){
	uint8_t c = input;
	bool valid = false;
	if(escaping){
		switch(input){
			case ESC_END:
				c = END;
				valid = true;
			break;
			case ESC_ESC:
				c = ESC;
				valid = true;
			break;
		}
		escaping = false;
	}
	else {
		switch (c){
		case END:
			if (num_chars > 0){
				complete = true;
			}
			break;

		case ESC:
			escaping = true;
			break;

		default:
			valid = true;
		}
	}
	if(valid){
		slip_rx_buffer[num_chars++] = c;
	}
}

bool slip_rx_is_complete(){
	if (complete){
		escaping = false;
		num_chars=0;
		complete = false;
		return true;
	}
	return false;
}

void slip_init(){
	num_chars = 0;
	escaping = 0;
	complete = 0;
}
